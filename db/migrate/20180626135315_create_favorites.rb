class CreateFavorites < ActiveRecord::Migration[5.2]
  def change
    create_table :favorites do |t|
      t.integer :user_id, null: false
      t.integer :sound_id, null: false
      t.integer :folder_id

      t.timestamps
    end

    add_foreign_key :users, :favorites
    add_foreign_key :sounds, :favorites
    add_foreign_key :folders, :favorites
  end
end
