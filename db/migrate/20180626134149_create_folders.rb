class CreateFolders < ActiveRecord::Migration[5.2]
  def change
    create_table :folders do |t|
      t.string :name, null: false
      t.integer :folder_id
      t.integer :user_id

      t.timestamps
    end

    add_foreign_key :folders, :folders
    add_foreign_key :users, :folders
  end
end
