# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.create!(name: 'Mariusz')
Sound.create!(name: 'Sound1')
Sound.create!(name: 'Sound2')
Sound.create!(name: 'Sound3')
Sound.create!(name: 'Sound4')

root1 = Folder.create!(name: 'Root1', user: user)
Folder.create!(name: 'Root2', user: user)
child1 = Folder.create!(name: 'Child1', folder: root1, user: user)
child2 = Folder.create!(name: 'Child2', folder: child1, user: user)
Folder.create!(name: 'Child2', folder: child2, user: user)
