class SoundSerializer < ActiveModel::Serializer
  attributes :id, :name
end
