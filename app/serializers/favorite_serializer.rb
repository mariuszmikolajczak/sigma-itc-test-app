class FavoriteSerializer < ActiveModel::Serializer
  attributes :id

  belongs_to :user
  belongs_to :sound
  belongs_to :folder
end
