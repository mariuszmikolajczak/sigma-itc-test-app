module V1
  class ApplicationController < ::ApplicationController
    def current_user
      # Mock for current_user
      User.first
    end
  end
end
