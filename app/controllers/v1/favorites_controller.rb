module V1
  class FavoritesController < ApplicationController
    def initialize
      super
      @favorites_service = FavoritesService.new(current_user)
    end

    def index
      render json: favorites_service.list
    end

    def create
      @favorite = favorites_service.create(params[:sound_id], params[:folder_id])
      render_favorite
    end

    def show
      @favorite = favorites_service.find(params[:id])
      render_favorite
    end

    def update
      @favorite = favorites_service.update(params[:id], params[:folder_id])
      render_favorite
    end

    def destroy
      @favorite = favorites_service.find(params[:id])
      @favorite.destroy!
      render_favorite
    end

    private

    attr_reader :favorites_service

    def render_favorite
      render json: @favorite
    end
  end
end
