class FavoritesService
  def initialize(user)
    @user = user
  end

  def create(sound_id, folder_id)
    sound = Sound.find(sound_id)
    folder = Folder.find_by_id(folder_id)
    user.favorites.create!(sound: sound, folder: folder)
  end

  def update(favorite_id, folder_id)
    folder = folder_id.present? ? Folder.find(folder_id) : nil
    favorite = find(favorite_id)
    favorite.update!(folder: folder)
    favorite
  end

  def find(favorite_id)
    user.favorites.find(favorite_id)
  end

  def list
    user.favorites
  end

  private

  attr_reader :user
end
