class Folder < ApplicationRecord
  has_many :folders
  belongs_to :folder, optional: true
  belongs_to :user

  def path
    "#{folder.try(:path)}/#{name}"
  end
end
