class Favorite < ApplicationRecord
  belongs_to :user
  belongs_to :folder, optional: true
  belongs_to :sound

  validate :one_favorite_per_user_song, on: :create

  def one_favorite_per_user_song
    errors.add(:one_favorite_per_user_song, ': User can favorite only one sound') if Favorite.exists?(sound: sound,
                                                                                                      user: user)
  end
end
