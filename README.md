# README

## Instructions to run

* Install rvm and ruby 2.5.1

* `rails db:migrate`

* `rails db:seed`

* `rails s`

* `rails routes` to see what endpoints are available
