FactoryBot.define do
  factory :folder do
    name Faker::Witcher.location

    association :user
  end
end
