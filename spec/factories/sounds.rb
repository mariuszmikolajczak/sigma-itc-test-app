FactoryBot.define do
  factory :sound do
    name Faker::Artist.name
  end
end
