require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'Associations' do
    it { should have_many(:favorites) }
    it { should have_many(:folders) }
  end
end
