require 'rails_helper'

RSpec.describe Sound, type: :model do
  describe 'Associations' do
    it { should have_many(:favorites) }
  end
end
