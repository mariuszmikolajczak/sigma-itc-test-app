require 'rails_helper'

RSpec.describe Folder, type: :model do
  before(:all) do
    @folder = create(:folder)
    @user = create(:user)
  end

  subject { described_class.new(user: @user, folder: @folder) }

  describe 'Validations' do
    context 'not valid' do
      it 'without user' do
        subject.user_id = nil
        expect(subject).to_not be_valid
      end
    end

    context 'valid' do
      it 'with valid attributes' do
        expect(subject).to be_valid
      end

      it 'without folder' do
        subject.folder_id = nil
        expect(subject).to be_valid
      end
    end
  end

  describe 'Associations' do
    it { should have_many(:folders) }
    it { should belong_to(:folder) }
    it { should belong_to(:user) }
  end
end
