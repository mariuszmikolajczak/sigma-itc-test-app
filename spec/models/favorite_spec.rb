require 'rails_helper'

RSpec.describe Favorite, type: :model do
  before(:all) do
    @user = create(:user)
    @sound = create(:sound)
    @folder = create(:folder)
  end

  subject { described_class.new(user: @user, sound: @sound, folder: @folder) }

  describe 'Validations' do
    context 'not valid' do
      it 'without user' do
        subject.user_id = nil
        expect(subject).to_not be_valid
      end

      it 'without sound' do
        subject.sound_id = nil
        expect(subject).to_not be_valid
      end

      it 'when favorite exists' do
        described_class.create(user: @user, sound: @sound)
        expect(subject).to_not be_valid
      end
    end

    context 'valid' do
      it 'with valid attributes' do
        expect(subject).to be_valid
      end

      it 'without folder' do
        subject.folder_id = nil
        expect(subject).to be_valid
      end
    end
  end

  describe 'Associations' do
    it { should belong_to(:user) }
    it { should belong_to(:folder) }
    it { should belong_to(:sound) }
  end
end
